# crater_accumulator

Houdini 18 HDA that simulates the accumulation of craters on a terrain, such as Earth's Moon, Mars or an asteroid.
Accepts a CVS file with asteroid diameter as the first field. Example uses NEOs with known diameters in km.

![Flipbook of crater accumulator](flipbook/CraterAccumulator.012.mov)